// 1 - Import de puppeteer
const puppeteer = require('puppeteer')

/*
// 2 - Récupération des URLs de toutes les pages à visiter
- waitFor("body"): met le script en pause le temps que la page se charge
- document.querySelectorAll(selector): renvoie tous les noeuds qui vérifient le selecteur
- [...document.querySelectorAll(selector)]: caste les réponses en tableau
- Array.map(link => link.href): récupère les attributs href de tous les liens
*/
const getAllUrl = async browser => {
    const page = await browser.newPage()
    await page.setViewport({ width : 1000, height : 1200 })
    await page.goto('https://www.fr.lastminute.com/sejour')

    await page.focus('div > select#holidays-search-to');
    await page.keyboard.type('Polynésie française');

    await page.focus('div > select#holidays-search-from');
    await page.keyboard.type('Lyon');

    await page.focus('div > select#holidays-duration');
    await page.keyboard.type("Peu m'importe");

    await page.click('div.hidden-xs > button.btn');

    await page.waitFor('body')
    const result = await page.evaluate(() => [...document.querySelectorAll('h3.title-main > a')].map(link => link.href), )
    return result
}

// 3 - Récupération du prix et du tarif d'un livre à partir d'une url (voir exo #2)
const getDataFromUrl = async (browser, url) => {
    const page = await browser.newPage()
    await page.goto(url)
    await page.waitFor('body')
    return page.evaluate(() => {
        let hotel = document.querySelector('h1.h1-like').innerText;
        let prix = document.querySelector('div.price > span').innerText;
        let datedepartpossible = document.querySelector('div#availabilitiesList > div > div.list > div.item > div:nth-child(1)').innerText;
        //let description_titre = document.querySelector('div#subDetailContent_2\\\\div').innerText;
        //let description = document.querySelector('div#subDetailContent_2 > p:nth-child(1)').innerHTML;
        return {
            hotel,
            prix,
            datedepartpossible,
            //description_titre,
            //description,
        }
    })
}

/*
// 4 - Fonction principale : instanciation d'un navigateur et renvoi des résultats
- urlList.map(url => getDataFromUrl(browser, url)):
appelle la fonction getDataFromUrl sur chaque URL de `urlList` et renvoi un tableau

- await Promise.all(promesse1, promesse2, promesse3):
bloque de programme tant que toutes les promesses ne sont pas résolues
*/
const scrap = async () => {
    const browser = await puppeteer.launch({
        headless: false
    })
    const urlList = await getAllUrl(browser)
    const results = await Promise.all(
        urlList.map(url => getDataFromUrl(browser, url)),
    )
    browser.close()
    return results
}

// 5 - Appel la fonction `scrap()`, affichage les résulats et catch les erreurs
scrap()
    .then(value => {
        console.log(value)
    })
    .catch(e => console.log(`error: ${e}`))